# README #
======

[Test link contains Jasmine for unit-test](https://jsfiddle.net/rain_tuan/hg7yma68/16/)

[Test link without unit-test](http://codepen.io/rainism/pen/gWxjzV)


___

##Step by step thinking process to solve the issue

###Given the array

####[7 5 2 1]
####[9 6 6]
####[1]

###Step 1: 
- First largest sum will always be the values from the first columnn of each row
- 7 + 9 + 1 => 17 Pick
		
###Step 2:
- Due to each row was ordered descending already so next possible largest sum may only consist of the adjacent value of the pre-picked ones (7 is 5), (9 is 6) (1 is null)
- New array that will contain next possible largest sums will be (call this array P1)
####[7, 5]
####[9, 6]
####[1]

###Step 3:
- Find all possible combinations of sum from the newly-found array P1
- 7 + 9 + 1 => 17 Used
- 5 + 9 + 1 => 15 Pick
- 7 + 6 + 1 => 14
- 5 + 6 + 1 => 14

- 17 was picked before so the next largest sum will be 15 (5 + 9 + 1)
- next possible largest sum may only consist of the adjacent value of the pre-picked ones (5 is 2)
- new array that will contain next possible largest sums will be (call this array P2)
####[7, 5, 2]
####[9, 6]
####[1]
		   
###Step 4:
- Find all possible combinations of sum from the newly-found array P2
- 7 + 9 + 1 => 17 Used
- 5 + 9 + 1 => 15 Used
- 7 + 6 + 1 => 14 Pick
- 5 + 6 + 1 => 14
- 2 + 9 + 1 => 12
- 2 + 6 + 1 => 9

- 17 and 15 were picked before so the next largest sum will be 14 (7 + 6 + 1)
- next possible largest sum may only consist of the adjacent value of the pre-picked ones (6 is 6)
- new array that will contain next possible largest sums will be (call this array P3)
####[7, 5, 2]
####[9, 6, 6]
####[1]
		   
###Step 5: => Repeat till we find the exact number of largest sums